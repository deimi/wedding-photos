# Wedding Photos

Guard page for our wedding photos

# Development

Use openssl in your terminal to create the encrypted web address 

```bash
openssl enc -aes-256-cbc -pbkdf2 -iter 5000 -salt -in unencrypted_webpage.txt -out encrypted_webpage.txt -pass file:password.txt -e -base64
```

**IMPORTANT**: The url needs to be with "http(s)://" or the validation check after the decryption will fail
